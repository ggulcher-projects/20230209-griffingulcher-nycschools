package com.example.nychighschools.api.repository

import com.example.nychighschools.api.models.SchoolResponse
import com.example.nychighschools.api.models.SchoolSATResponse

interface SchoolRequestsManager {

    suspend fun getSchools(): List<SchoolResponse>

    suspend fun getSchoolsSAT(schoolDbn: String): List<SchoolSATResponse>

}
