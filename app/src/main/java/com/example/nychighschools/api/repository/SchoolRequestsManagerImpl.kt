package com.example.nychighschools.api.repository

import com.example.nychighschools.api.ApiService
import com.example.nychighschools.api.models.SchoolResponse
import com.example.nychighschools.api.models.SchoolSATResponse
import javax.inject.Inject

class SchoolRequestsManagerImpl @Inject constructor(
    private val apiService: ApiService
) : SchoolRequestsManager {

    override suspend fun getSchools(): List<SchoolResponse> {
        return apiService.getSchools()
    }

    override suspend fun getSchoolsSAT(schoolDbn: String): List<SchoolSATResponse> {
        return apiService.getSchoolsSAT(schoolDbn)
    }
}
