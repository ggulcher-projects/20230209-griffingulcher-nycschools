package com.example.nychighschools.api

import com.example.nychighschools.api.models.SchoolResponse
import com.example.nychighschools.api.models.SchoolSATResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("s3k6-pzi2.json")
    suspend fun getSchools() : List<SchoolResponse>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolsSAT(
        @Query("dbn") schoolDbn: String
    ) : List<SchoolSATResponse>
}
