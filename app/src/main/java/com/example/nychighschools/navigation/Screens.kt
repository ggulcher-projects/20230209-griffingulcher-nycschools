package com.example.nychighschools.navigation

sealed class Screens(
    val route: String,
    val title: String? = null,
) {
    object SchoolsList : Screens(
        route = "schools_list",
        title = "Home"
    )

    object SchoolsSAT : Screens(
        route = "schools_sat",
        title = "Detail"
    )

}
