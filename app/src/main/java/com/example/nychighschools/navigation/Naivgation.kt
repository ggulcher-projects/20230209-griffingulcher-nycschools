package com.example.nychighschools.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.nychighschools.ui.schools.compose.SchoolListCompose
import com.example.nychighschools.ui.sat.compose.SatDetailCompose

/**
 * Jetpack Navigation Args is a lightweight means of passing
 * arguments between views
 *
 * Given more complex data sets, I may implement sharedPreferences
 * closer to the view layer
 */
@Composable
fun Navigation() {

    val navController: NavHostController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = Screens.SchoolsList.route
    ) {
        composable(
            route = Screens.SchoolsList.route
        ) {
            SchoolListCompose(navController = navController)
        }

        // Each navArgument relates to a saved variable and added to this pseudo-deep link
        composable(
            route = Screens.SchoolsSAT.route + "/{schoolDbn}/{schoolName}/{phoneNumber}/{address}",
            arguments = listOf(
                navArgument("schoolName") {
                    type = NavType.StringType
                },
                navArgument("phoneNumber") {
                    type = NavType.StringType
                },
                navArgument("address") {
                    type = NavType.StringType
                }
            )
        ) {
            val schoolName = it.arguments?.getString("schoolName")
            val phoneNumber = it.arguments?.getString("phoneNumber")
            val schoolAddress = it.arguments?.getString("address")

            SatDetailCompose(
                name = schoolName,
                phone = phoneNumber,
                address = schoolAddress
            )
        }
    }
}
