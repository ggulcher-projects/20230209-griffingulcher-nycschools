package com.example.nychighschools.ui.schools.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nychighschools.api.models.SchoolResponse
import com.example.nychighschools.api.repository.SchoolRequestsManagerImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolsViewModel @Inject constructor(
    private val repository: SchoolRequestsManagerImpl
) : ViewModel() {

    var state by mutableStateOf(SchoolState())

    init {
        getSchools()
    }

    private fun getSchools() {
        viewModelScope.launch {
            // TODO - With larger data sets, a Resource or RxJava would be used to handle specific exceptions
            kotlin.runCatching {
                repository.getSchools().let { result ->
                    state = state.copy(schools = result, isLoading = false)
                }
            }
        }
    }

    data class SchoolState(
        val schools: List<SchoolResponse> = emptyList(),
        val isLoading: Boolean = true
    )
}
