package com.example.nychighschools.ui.sat.compose

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.nychighschools.ui.sat.compose.components.SatHeader
import com.example.nychighschools.ui.sat.compose.components.ScoreCard
import com.example.nychighschools.ui.sat.viewmodel.SatViewModel

/**
 * SAT Detail composable.
 *
 * @param name School Name value passed from NavArgs
 * @param phone Phone Number value passed from NavArgs
 * @param address Address value passed from NavArgs
 *
 */
@Composable
fun SatDetailCompose(
    viewModel: SatViewModel = hiltViewModel(),
    name: String?,
    phone: String?,
    address: String?
) {
    val state = viewModel.state

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(10.dp)
    ) {

        SatHeader(name = name, phone = phone, address = address)

        ScoreCard(state = state)
    }
}
