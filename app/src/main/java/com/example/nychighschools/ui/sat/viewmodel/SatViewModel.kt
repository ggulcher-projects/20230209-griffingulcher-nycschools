package com.example.nychighschools.ui.sat.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nychighschools.api.models.SchoolSATResponse
import com.example.nychighschools.api.repository.SchoolRequestsManagerImpl
import com.example.nychighschools.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SatViewModel @Inject constructor(
    private val repository: SchoolRequestsManagerImpl,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    var state by mutableStateOf(SatState())

    init {
        // TODO - This is where the shared navArg for the chosen school's DBN goes
        savedStateHandle.get<String>(Constants.PARAM_SAT_ID)?.let { schoolDbn ->
            getSchoolsSAT(schoolDbn)
        }
    }

    private fun getSchoolsSAT(schoolDbn: String) {
        viewModelScope.launch {
            // TODO - With larger data sets, a Resource or RxJava would be used to handle specific exceptions
            kotlin.runCatching {
                repository.getSchoolsSAT(schoolDbn).let { result ->
                    state = state.copy(schoolSATs = result[0])
                }
            }
        }
    }

    data class SatState(
        val schoolSATs: SchoolSATResponse? = null
    )
}
