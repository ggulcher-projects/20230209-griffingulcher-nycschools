package com.example.nychighschools.ui.schools.compose

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.nychighschools.ui.schools.compose.components.SchoolListItem
import com.example.nychighschools.ui.schools.compose.components.SearchBar
import com.example.nychighschools.ui.schools.compose.components.TopBar
import com.example.nychighschools.ui.schools.viewmodel.SchoolsViewModel

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SchoolListCompose(
    viewModel: SchoolsViewModel = hiltViewModel(),
    navController: NavController
) {

    val scaffoldState = rememberScaffoldState()
    val searchState = remember { mutableStateOf(TextFieldValue("")) }
    val lazyListState = rememberLazyListState()
    val state = viewModel.state

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            // TODO - Add to Constants to replace hard coded strings, ideally values from API
            TopBar(title = "NYC School Directory")
        },
    ) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            Column(
                modifier = Modifier.padding(12.dp),
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                val isBeingSearched = searchState.value.text
                SearchBar(
                    hint = "...",
                    modifier = Modifier.fillMaxWidth(),
                    state = searchState
                )
                if (state.isLoading) {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier.fillMaxSize()
                    ) {
                        CircularProgressIndicator()
                    }
                } else {
                    LazyColumn(
                        state = lazyListState,
                        verticalArrangement = Arrangement.spacedBy(10.dp)
                    ) {
                        items(
                            items = state.schools.filter {
                                it.schoolName.contains(isBeingSearched, ignoreCase = true)
                            },
                            key = { it.dbn }
                        ) { schools ->
                            SchoolListItem(
                                school = schools,
                                navController = navController
                            )
                        }
                    }
                }
            }
        }
    }
}
