package com.example.nychighschools.ui.sat.compose.components

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.nychighschools.ui.sat.viewmodel.SatViewModel

/**
 * The SAT Score Card composable.
 *
 * Objects divided for more organized composables
 *
 */
@Composable
fun ScoreCard(
    state: SatViewModel.SatState
) {

    Text(
        text = "SAT Scores:",
        fontWeight = FontWeight.Bold,
    )

    Row(modifier = Modifier
        .fillMaxWidth()
        .border(
            border = ButtonDefaults.outlinedBorder,
            shape = RoundedCornerShape(12.dp)
        )
    ) {
        Box(
            modifier = Modifier.weight(1f)
        ) {
            ScoreCardItem(
                title = "Math",
                score = state.schoolSATs?.satMathAvgScore ?: "Unavailable"
            )
        }
        Box(
            modifier = Modifier.weight(1f)
        ) {
            ScoreCardItem(
                title = "Writing",
                score = state.schoolSATs?.satWritingAvgScore ?: "Unavailable"
            )
        }
        Box(
            modifier = Modifier.weight(1f)
        ) {
            ScoreCardItem(
                title = "Reading",
                score = state.schoolSATs?.satCriticalReadingAvgScore ?: "Unavailable"
            )
        }
    }
}

@Composable
fun ScoreCardItem(
    title: String,
    score: String
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = title,
            style = MaterialTheme.typography.body1,
            fontWeight = FontWeight.Bold
        )
        Text(text = score)
    }
}
