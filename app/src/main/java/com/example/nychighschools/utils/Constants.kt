package com.example.nychighschools.utils

object Constants {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    const val PARAM_SAT_ID = "schoolDbn"
}