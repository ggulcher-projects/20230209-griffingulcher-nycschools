package com.example.nychighschools.di

import com.example.nychighschools.api.ApiService
import com.example.nychighschools.api.repository.SchoolRequestsManager
import com.example.nychighschools.api.repository.SchoolRequestsManagerImpl
import com.example.nychighschools.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesApiService(): ApiService {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun providesRepository(
        api: ApiService
    ): SchoolRequestsManager {
        return SchoolRequestsManagerImpl(api)
    }
}
